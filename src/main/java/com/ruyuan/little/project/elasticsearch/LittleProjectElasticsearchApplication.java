package com.ruyuan.little.project.elasticsearch;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@SpringBootApplication
@MapperScan("com.ruyuan.little.project.elasticsearch.biz.admin.mapper")
public class LittleProjectElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittleProjectElasticsearchApplication.class, args);
    }

}
